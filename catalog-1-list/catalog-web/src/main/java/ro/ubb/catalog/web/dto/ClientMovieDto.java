package ro.ubb.catalog.web.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ClientMovieDto extends BaseDto {
    private long clientId;
    private long movieId;
    private String movieName;
    private String startDate;

    @Override
    public String toString() {
        return "Rent{" +
                "rentedToClientId='" + clientId + '\'' +
                ", movieRentedId='" + movieId + '\'' +
                ", startDate=" + startDate +
                "} " + super.toString();
    }
}
