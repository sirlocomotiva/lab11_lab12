package ro.ubb.catalog.web.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MovieDto extends BaseDto {
    private String name;
    private int timesRented;

    public String toString() {
        return "MovieDto{" +
                "name='" + name + '\'' +
                ", timesRented='" + timesRented +
                "} " + super.toString();
    }
}
