package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.service.ClientService;
import ro.ubb.catalog.web.converter.ClientMovieConverter;
import ro.ubb.catalog.web.dto.ClientMovieDto;

import java.util.*;

/**
 * Created by radu.
 */

@RestController
public class RentController {
    private static final Logger log = LoggerFactory.getLogger(RentController.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientMovieConverter clientMovieConverter;


    @RequestMapping(value = "/rents/{clientId}", method = RequestMethod.GET)
    public Set<ClientMovieDto> getClientMovie(
            @PathVariable final Long studentId) {
        log.trace("getStudentDisciplines: studentId={}", studentId);

        throw new RuntimeException("not yet implemented");
    }

    @RequestMapping(value = "/rents/{clientId}", method = RequestMethod.PUT)
    public Set<ClientMovieDto> updateStudentGrades(
            @PathVariable final Long studentId,
            @RequestBody final Set<ClientMovieDto> clientMovieDtos) {
        log.trace("updateStudentGrades: studentId={}, studentDisciplineDtos={}",
                studentId, clientMovieDtos);

        throw new RuntimeException("not yet implemented");
    }

}
