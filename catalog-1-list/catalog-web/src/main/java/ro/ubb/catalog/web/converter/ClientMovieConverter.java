package ro.ubb.catalog.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.ClientMovie;
import ro.ubb.catalog.web.dto.ClientMovieDto;

import javax.management.RuntimeErrorException;

/**
 * Created by radu.
 */
@Component
public class ClientMovieConverter
        extends AbstractConverter<ClientMovie, ClientMovieDto> {

    @Override
    public ClientMovie convertDtoToModel(ClientMovieDto clientMovieDto) {
        throw new RuntimeException("not yet implemented");
    }

    @Override
    public ClientMovieDto convertModelToDto(ClientMovie clientMovie) {
        return ClientMovieDto.builder()
                .clientId(clientMovie.getClient().getId())
                .movieId(clientMovie.getMovie().getId())
                .movieName(clientMovie.getMovie().getName())
                .startDate(clientMovie.getStartDate())
                .build();
    }
}
