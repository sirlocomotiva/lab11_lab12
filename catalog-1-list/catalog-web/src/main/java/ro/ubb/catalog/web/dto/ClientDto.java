package ro.ubb.catalog.web.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ClientDto extends BaseDto {
    private String cnp;
    private String name;

    public String toString() {
        return "ClientDto{" +
                "cnp='" + cnp + '\'' +
                ", name='" + name +
                "} " + super.toString();
    }
}
