package ro.ubb.catalog.core.service;

import ro.ubb.catalog.core.model.Client;

import java.util.List;

public interface ClientService {
    public List<Client> getAllClients();
    public Client addClient(Client c);
    public Client updateClient(Long id, Client c);
    void deleteClient(Long id);
}
