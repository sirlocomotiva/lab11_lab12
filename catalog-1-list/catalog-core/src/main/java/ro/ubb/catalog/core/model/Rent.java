package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Rent extends BaseEntity<Long> {
    private long rentedToClientId;
    private long movieRentedId;
    private String startDate;

    @Override
    public String toString() {
        return "Rent{" +
                "rentedToClientId='" + rentedToClientId + '\'' +
                ", movieRentedId='" + movieRentedId + '\'' +
                ", startDate=" + startDate +
                "} " + super.toString();
    }
}
