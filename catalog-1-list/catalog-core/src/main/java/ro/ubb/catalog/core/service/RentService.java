package ro.ubb.catalog.core.service;

import java.util.List;
import ro.ubb.catalog.core.model.*;

public interface RentService {
    public List<ClientMovie> getAllRents();
}
