package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Movie extends  BaseEntity<Long> {
    private String name;
    private int timesRented;

    @Override
    public String toString() {
        return "Movie{" +
                "name='" + name + '\'' +
                ", timesRented='" + timesRented +
                "} " + super.toString();
    }
}
