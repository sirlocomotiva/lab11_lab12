package ro.ubb.catalog.core.service;

import ro.ubb.catalog.core.model.Movie;

import java.util.List;

public interface MovieService {
    public List<Movie> getAllMovies();
    public Movie addMovie(Movie c);
    public Movie updateMovie(Long id, Movie c);
    public void deleteMovie(Long id);
}
